# Digital grunnpakke

Målet med dette prosjektet er å samle forslag til digital grunnpakke i en bedrift.

## Dette er på plass nå

[sjekkliste-ny-kontorplass.md](sjekkliste-ny-kontorplass.md)

## Dette er halvferdig

[sjekkliste-ny-android-telefon.md](sjekkliste-ny-android-telefon.md)  
[sjekkliste-ny-android-telefon.md](sjekkliste-ny-android-telefon.md)  

## Dette står på ønskelisten

- Forslag tilgangskontroll (fysisk og til IT)
- Sjekkliste for ny Windows-maskin
- Sjekkliste for etablering av Gitlab-server
