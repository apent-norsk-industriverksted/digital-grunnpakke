# VM Host

## Spesifikasjon hardware

- minst 64GB ECC-RAM
- minst CPU: Xeon Silver 8 kjerner eller tilsvarende
- minst 1 SSD-disk 128GB til operativsystem
- minst 2 disker 8TB skal kunne kjøres uten RAID. (Velger å bruke ZFS) OBS! Harddisker som er av typen SMR er ubrukelige i praksis.
- minst 1 nettverkskort, 1000Gbps

# Oppsett av debian-maskin for backup

```
sudo apt-get update && sudo apt-get upgrade

sudo apt install zfsutils-linux
```

## Lag zfs pool

```
# ZFS bruker kun hele harddisker. Se etter disker.
sudo fdisk -l
# Lag et pool. OBS! bytt ut  /dev/sda /dev/sdb ved behov. Mirror er en god praksis.
sudo zpool create zfs1 mirror /dev/sda /dev/sdb
```

## Lag produksjonspool

```
sudo zfs create zfs1/local
hexdump -n 16 -e '4/4 "%08X" 1 "\n"' /dev/urandom > /zfs1/local/prod.key
zfs create -o encryption=on -o keylocation=file:///zfs1/local/prod.key -o keyformat=passphrase zfs1/prod
```

## Sett opp snapshot og synk til ekstern backup

```
sudo apt install sanoid
```

Konfigurer automatisk backup ved hjelp av /etc/sanoid/sanoid.conf

```
[zfs1]
        use_template = production
#[zfs1]
#       use_template = production
#       recursive = yes
#       process_children_only = yes
#[data/images/win7]
#       hourly = 4

#############################
# templates below this line #
#############################

[template_production]
        frequently = 0
        hourly = 36
        daily = 30
        monthly = 3
        yearly = 0
        autosnap = yes
        autoprune = yes
```

TODO!

## Test at det er mulig å dekryptere på en annen maskin

```
zfs load-key -r banshee/encrypted
```
