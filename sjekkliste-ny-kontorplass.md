# Sjekkliste for ny kontorplass

- [ ] Søppelbøtte
- [ ] Bord
- [ ] Skjermer+skjermfeste
- [ ] Dockingstasjon
- [ ] Strøm
- [ ] Nett

## Heve-senkebord

Her ble det vanskelig å få ut konkrete delenummer som kan bestilles. Bord og matte kom ferdig montert.

- Produsent: EFG
- Leverandør: Kontorplan AS

> Izi Pro Arbeidsbord
> * L: 1800 D: 800 H: 640-1290 mm
> * Bord med elektrisk hev/senk
> * Inkludert HalfPipekabelrenne og strømforgrener med fire uttak
> * Bordplate: Laminat bjørk (MB)
> * Understell:Pulverlakkert stål svart (24) Kaberenne: Svart

- Produsent: Götessons
- Leverandør: Kontorplan AS

> Dot Standzon
> * Ø: 600 mm
> * Myk og ergonomisk matte
> * Kjerne: absorberende/lyddempende polyuretan
> * Overflate: Kork


## PC

### Spesifikasjon

- Grafikkprosessor: NVIDIA GeForce *eller NVIDIA Quadro*
- min 128GB SSD
- min 8GB RAM
- min 2 USB A ports
- min 1 USB C port
- Ethernet connector inside of PC
- norwegian keyboard, numeric keypad

### Forslag

- Produsentens delenummer: 2ZC56EA#UUW
- Komplett.no delenummer 1112255

## Mus for 3D-grafikk

- 3Dconnexion SpaceMouse Compact
- Produsentens delenummer: 3DX-700059
- Komplett.no delenummer: 1048500

## Dockingstasjon

- HP Elite 230W Thunderbolt Dock
- Produsentens delenummer: 3TR87AA#ABB
- Komplett.no delenummer:  1099905

## 2 stk skjermer

- Philips 32" curved skjerm
- Produsentens delenummer: 325E1C/00
- Komplett.no delenummer:  1138848

## Skjermfeste

- KENSON TWIN Monitor gassarm 10046SI
- Produsentens delenummer: 10046SI
- Komplett.no delenummer:  1118837
